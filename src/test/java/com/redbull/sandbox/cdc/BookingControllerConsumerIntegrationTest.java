package com.redbull.sandbox.cdc;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import com.redbull.sandbox.cdc.api.LoggedResponse;
import com.redbull.sandbox.cdc.api.NewLogRequest;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.util.SocketUtils;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class SecurityControllerConsumerIntegrationTest {

    private final int randomizedPort = SocketUtils.findAvailableTcpPort();

    @Rule
    public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2(
            "logging_provider",
            "localhost",
            randomizedPort,
            this
    );

    @Pact(consumer = "logging_consumer", provider = "logging_provider")
    public RequestResponsePact createPact(PactDslWithProvider builder) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        return builder
                .given("logging an event")
                .uponReceiving("post /log request")
                    .headers("Content-Type", "application/json")
                    .path("/log")
                    .method("POST")
                    .body("{\"level\": \"INFO\", \"message\": \"User Pawel is authorized.\"}")
                .willRespondWith()
                    .status(200)
                    .headers(headers)
                    .body("{\"message\": \"New log event with level 'INFO' was accepted.\"}")
                .toPact();
    }

    @Test
    @PactVerification()
    public void should_log_message() {
        // given
        String loggingUrl = mockProvider.getUrl() + "/log";

        // when
        final LoggedResponse loggedResponse = new RestTemplate().postForObject(
                loggingUrl,
                new NewLogRequest("INFO", "User Pawel is authorized."),
                LoggedResponse.class
        );

        // then
        assertThat(loggedResponse.getMessage(), equalTo("New log event with level 'INFO' was accepted."));
    }
}
