package com.redbull.sandbox.cdc.security;

import com.redbull.sandbox.cdc.api.AuthorizationResponse;
import com.redbull.sandbox.cdc.api.AuthorizeUserRequest;
import com.redbull.sandbox.cdc.api.LoggedResponse;
import com.redbull.sandbox.cdc.api.NewLogRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RestController
public class SecurityController {

    private final static Logger LOGGER = LoggerFactory.getLogger(SecurityController.class);

    private final List<String> allowedUsers = Arrays.asList("Pawel", "Sasha", "Piotr");
    private final RestTemplate restTemplate;
    private final String loggingUrl;

    @Autowired
    public SecurityController(RestTemplate restTemplate,
                              @Value(value = "${operation.logging.url}") String loggingUrl) {
        this.loggingUrl = loggingUrl;
        this.restTemplate = restTemplate;
    }

    @RequestMapping(value = "/authorize", method = RequestMethod.POST)
    public AuthorizationResponse authorizeUser(@RequestBody AuthorizeUserRequest request) {
        final String user = request.getUser();
        LOGGER.info("Attempting to authorize user '{}'.", user);
        if (allowedUsers.contains(user)) {
            log("INFO", "User " + user + " is authorized.");
            return new AuthorizationResponse(user, "allowed");
        }
        log("WARN", "User " + user + " is not authorized.");
        return new AuthorizationResponse(user, "not-recognized");
    }

    private LoggedResponse log(String level, String message) {
        final LoggedResponse response = restTemplate.postForObject(
                loggingUrl,
                new NewLogRequest(level, message),
                LoggedResponse.class);
        LOGGER.info("Successfully logged event '{}'.", response.getMessage());
        return response;
    }
}
