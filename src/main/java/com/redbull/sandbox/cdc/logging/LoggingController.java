package com.redbull.sandbox.cdc.logging;

import com.redbull.sandbox.cdc.api.LoggedResponse;
import com.redbull.sandbox.cdc.api.NewLogRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoggingController {

    private final static Logger LOGGER = LoggerFactory.getLogger(LoggingController.class);

    @RequestMapping(value = "/log", method = RequestMethod.POST)
    public LoggedResponse log(@RequestBody NewLogRequest request) {
        final String level = request.getLevel();
        LOGGER.info("Logging new event with level '{}' and message '{}'.",
                level, request.getMessage());
        return new LoggedResponse("New log event with level '" + level + "' was accepted.");
    }
}
