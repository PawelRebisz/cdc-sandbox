package com.redbull.sandbox.cdc.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorizationResponse {
    private final String user;
    private final String status;

    @JsonCreator
    public AuthorizationResponse(@JsonProperty(value = "user") String user,
                                 @JsonProperty(value = "status") String status) {
        this.user = user;
        this.status = status;
    }

    public String getUser() {
        return user;
    }

    public String getStatus() {
        return status;
    }
}
