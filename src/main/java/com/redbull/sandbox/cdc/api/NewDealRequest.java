package com.redbull.sandbox.cdc.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NewDealRequest {

    private final String dealName;
    private final String user;

    @JsonCreator
    public NewDealRequest(@JsonProperty(value = "dealName") String dealName,
                          @JsonProperty(value = "user") String user) {
        this.dealName = dealName;
        this.user = user;
    }

    public String getDealName() {
        return dealName;
    }

    public String getUser() {
        return user;
    }
}
