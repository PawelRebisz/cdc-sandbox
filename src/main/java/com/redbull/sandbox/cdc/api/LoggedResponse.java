package com.redbull.sandbox.cdc.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LoggedResponse {
    private final String message;

    @JsonCreator
    public LoggedResponse(@JsonProperty(value = "message") String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
