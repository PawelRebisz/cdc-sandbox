package com.redbull.sandbox.cdc.api;

import java.util.UUID;

public class BookingResponse {

    private final String dealName;
    private final long amount;
    private final UUID uuid;

    public BookingResponse(String dealName, long amount, UUID uuid) {
        this.dealName = dealName;
        this.amount = amount;
        this.uuid = uuid;
    }

    public String getDealName() {
        return dealName;
    }

    public long getAmount() {
        return amount;
    }

    public UUID getUuid() {
        return uuid;
    }
}
