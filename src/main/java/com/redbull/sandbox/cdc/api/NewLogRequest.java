package com.redbull.sandbox.cdc.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NewLogRequest {

    private final String level;
    private final String message;

    @JsonCreator
    public NewLogRequest(@JsonProperty(value = "level") String level,
                         @JsonProperty(value = "message") String message) {
        this.level = level;
        this.message = message;
    }

    public String getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }
}
