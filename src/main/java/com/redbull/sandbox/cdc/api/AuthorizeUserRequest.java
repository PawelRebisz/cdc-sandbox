package com.redbull.sandbox.cdc.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorizeUserRequest {
    private final String user;

    @JsonCreator
    public AuthorizeUserRequest(@JsonProperty(value = "user") String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
