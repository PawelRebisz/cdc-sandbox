package com.redbull.sandbox.cdc.booking;

import com.redbull.sandbox.cdc.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class BookingController {

    private final static Logger LOGGER = LoggerFactory.getLogger(BookingController.class);
    private final AtomicLong counter = new AtomicLong();
    private final RestTemplate restTemplate;
    private final String loggingUrl;
    private String authorizeUrl;

    @Autowired
    public BookingController(RestTemplate restTemplate,
                             @Value(value = "${operation.logging.url}") String loggingUrl,
                             @Value(value = "${operation.authorization.url}") String authorizeUrl) {
        this.loggingUrl = loggingUrl;
        this.authorizeUrl = authorizeUrl;

        this.restTemplate = restTemplate;
    }

    @RequestMapping(value = "/book", method = RequestMethod.POST)
    public BookingResponse bookNextDeal(@RequestBody NewDealRequest request) {
        final String user = request.getUser();
        LOGGER.info("Booking new deal with name '{}' for user '{}'.", request.getDealName(), user);
        AuthorizationResponse authorization = authorizeUser(user);

        if ("allowed".equals(authorization.getStatus())) {
            log("INFO", "User " + user + " is valid.");
            return new BookingResponse(request.getDealName(), counter.incrementAndGet(), UUID.randomUUID());
        } else {
            log("ERROR", "User " + user + " is not great.");
            throw new IllegalStateException("User " + user + " is not allowed to make a booking.");
        }
    }

    private AuthorizationResponse authorizeUser(String user) {
        return restTemplate.postForObject(
                authorizeUrl,
                new AuthorizeUserRequest(user),
                AuthorizationResponse.class);
    }

    private LoggedResponse log(String level, String message) {
        final LoggedResponse response = restTemplate.postForObject(
                loggingUrl,
                new NewLogRequest(level, message),
                LoggedResponse.class);
        LOGGER.info("Successfully logged event '{}'.", response.getMessage());
        return response;
    }
}
